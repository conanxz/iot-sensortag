#!/usr/bin/env python
"""This script is a MQTT Client"""

import sys
import os
import logging
import paho.mqtt.client as paho

def setup_custom_logger(name):
    """Custom logger setup"""
    formatter = logging.Formatter(fmt='%(asctime)s.%(msecs)03d %(levelname)s '+
                                  '{%(module)s} [%(funcName)s] %(message)s',
                                  datefmt='%Y-%m-%d %H:%M:%S')
    handler = logging.FileHandler('mqtt-messages.log', mode='w')
    handler.setFormatter(formatter)
    screen_handler = logging.StreamHandler(stream=sys.stdout)
    screen_handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    logger.addHandler(screen_handler)
    return logger

LOG = setup_custom_logger("mqttClient")

# Read in configuration from environment variables
BROKER_URL = os.getenv('SENSORTAG_MQTT_URL')
BROKER_PORT = int(os.getenv('SENSORTAG_MQTT_PORT'))
CLIENT_NAME = os.getenv('SENSORTAG_MQTT_CLIENT_NAME')
TOPIC_NAME = os.getenv('SENSORTAG_MQTT_TOPIC')

CLIENT = paho.Client(CLIENT_NAME)

def on_message(client, userdata, message):
    """Callback on receving messages"""
    #pylint: disable=unused-argument
    str_msg = str(message.payload.decode("utf-8"))
    LOG.info(str_msg)

CLIENT.on_message = on_message

LOG.info("connecting to broker: %s", BROKER_URL)
CLIENT.connect(BROKER_URL, BROKER_PORT)
CLIENT.loop_start()
LOG.info("subscribing: %s", TOPIC_NAME)
CLIENT.subscribe(TOPIC_NAME)

COMMAND = ""
while COMMAND != "quit":
    COMMAND = input("#> ")

CLIENT.disconnect()
CLIENT.loop_stop()
