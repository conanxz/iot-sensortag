# iot-sensortag

A hackathon project for interacting with TI Sensortag CC2650

# MQTT Client

Environment variables

```
SENSORTAG_MQTT_URL=<broker_url>
SENSORTAG_MQTT_PORT=<broker_port>
SENSORTAG_MQTT_TOPIC=<topic_name>
SENSORTAG_MQTT_CLIENT_NAME=<client_name>
```

Run the MQTT Client

`python3 src/mqtt_client.py`

Quit the client

`#> quit`
